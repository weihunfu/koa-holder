module.exports = {
  // 登录验证码过去
  loginCode: {
    key: 'koaSessionLoginCode', // cookie key
    maxAge: 1000 * 60 * 1, // 1分钟。有效期，单位：ms 毫秒
    overwrite: true, // 是否允许重写
    httpOnly: true, // true 表示只有服务端可以获取cookie
    signed: true, // 是否签名
    rolling: false, // 是否每次响应时刷新Session的有效期
    renew: false, // 是否在Session快过期时刷新Session的有效期
  },
  otherSession: {}
}