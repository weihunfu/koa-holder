// 通用工具类
const log = require('./log4js')
module.exports = {
  // 监听请求
  info (msg) {
    log.info(msg)
  },
  // 成功回调
  success (code = 0, data = '', message = '操作成功') {
    log.debug(data)
    return { data, message, code }
  },
  // 失败回调
  error (code = -1, data = '', message = '操作失败') {
    log.error(data)
    return { data, message, code }
  },
  // 校验邮箱
  validateEmail (args) {
    const req = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/
    return req.test(args)
  },
  // 校验手机号
  validatePhone (args) {
    const req = /^1[3,4,5,6,7,8,9]\d{9}$/
    return req.test(args)
  },
  // 校验6到18位大小写字母数字下划线组成的密码
  validatePassword (args) {
    const reg = /^[a-zA-Z0-9_]{6,18}$/;
    return reg.test(args);
  }
}