const Koa = require("koa")
const { koaBody } = require('koa-body')
const session = require('koa-session')
const cors = require('koa2-cors')

// 接口路由
const router = require('./../router')
// 日志处理中间件
const { paramsWrite, appError } = require('./../middleware/logger.middleware')

const { SESSION_KEY } = require('./../config/config.default')
const { loginCode } = require('./../config/config.session')

const app = new Koa()

app.use(cors({
  credentials: true, // 证书
}))

app.keys = [SESSION_KEY] // cookie签名(session加密字段)
app.use(session(loginCode, app))

app.use(koaBody())
// 统一添加中间件 设置接口请求数据日志
app.use(paramsWrite)
// 错误处理
app.on('error', appError)

app
  .use(router.routes())
  .use(router.allowedMethods())

module.exports = app