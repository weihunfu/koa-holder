const fs = require('fs')
const Router = require('koa-router')

// 统一接口前缀
const router = new Router({ prefix: '/api' })

// 路由自动加载
fs.readdirSync(__dirname).forEach(file => {
  if (file !== 'index.js') {
    let r = require(`./${ file }`)
    router.use(r.routes())
  }
})
module.exports = router