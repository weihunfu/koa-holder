const Router = require('koa-router')
const svgCaptcha = require('svg-captcha')


// 工具类方法
const utils = require('../utils/utils')
// 请求前缀
const router = new Router({ prefix: '/users' })

// 验证码
router.get('/captcha/:id', async (ctx, next) => {
  let noise = Math.floor(Math.random() * 6)
  noise = noise ? noise : 1
  const captcha = svgCaptcha.createMathExpr({
    mathMin: 0, // 数学表达式的最小值
    mathMax: 99, // 数学表达式的最大值
    mathOperator: '+,-', // 要使用的运算符
    width: 120, // 宽
    height: 32, // 高
    fontSize: 44,  //验证码字体大小
    noise: noise, // 干扰线条的数量
  })
  const text = captcha.text.toLowerCase() // 验证码文字忽略大小写
  ctx.session.loginCaptcha = text // 存储到 session 里
  ctx.response.type = 'image/svg+xml' // 返回类型
  ctx.body = captcha.data
})

// 登录接口
router.post('/login', async (ctx, next) => {
  console.log(ctx.session.loginCaptcha)
  ctx.body = 'asd'
})

// 用户注册接口
router.post('/register', async (ctx, next) => {
  console.log(ctx.session.loginCaptcha)
  ctx.body = 'asd'
})

module.exports = router