// 成员密码表
const seq = require('./../db/seq.mysql')

const User = require('./user.base')

const UserPwd = seq.define('user_pwd', {
  user_id: {
    references: {
      model: User,
      key: user_id
    }
  }
})