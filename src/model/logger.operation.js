// 操作日志
const { DataTypes } = require('sequelize')

const seq = require('./../db/seq.mysql')

const User = require('./user.base')

const LoggerOperation = seq.define('logger_operation', {
  user_id: {
    allowNull: false,
    references: {
      model: User,
      key: user_id
    }
  },
  method: {
    type: DataTypes.CHAR(10),
    allowNull: false,
    comment: 'get, post, put or ...'
  },
  url: {
    type: DataTypes.STRING(),
    allowNull: false,
  }
})
module.exports = LoggerOperation