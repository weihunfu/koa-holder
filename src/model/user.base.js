// 成员基础信息表
const seq = require('./../db/seq.mysql')

const User = seq.define('logger_operation', {
  user_id: {
    primaryKey: true,
    unique: true,
    allowNull: false,
    comment: '用户ID, 主键'
  }
})
module.exports = User