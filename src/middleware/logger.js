const utils = require('../utils/utils')
// 接口请求参数写入
const paramsWrite = async (ctx, next) => {
  try {
    const { method, url } = ctx.request
    switch (method.toLowerCase()) {
      case 'get':
        utils.info(`method: ${method}, url: ${url}, data: ${JSON.stringify(ctx.request.query)}`)
        break;
      case 'post':
        utils.info(`method: ${method}, url: ${url}, data: ${JSON.stringify(ctx.request.body)}`)
        break;
      default:
        utils.info(`default params: ${ctx.request}`)
        break;
    }
  } catch (error) {
    utils.error(-1, '', `操作失败：${ error }`)
  }
  await next()
}

// app运行监听错误日志写入
const appError = async (err, ctx) => {
  ctx.body = utils.error(-1, `${ err.stack }`)
}

module.exports = {
  paramsWrite,
  appError
}