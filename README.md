# koa-holder
## 功能
- [x] 持久化日志存储 log4js
- [x] 数据精度处理

## 工具类方法
### 一、数据精度处理
```js
// 引用示例
const calculation = require('../utils/calculation')
// 使用
console.log(0.1 + 0.2, calculation.accAdd(0.1, 0.2)) // 加法。0.30000000000000004 0.3
console.log(0.3 - 0.2, calculation.accSub(0.3, 0.2)) // 减法。0.09999999999999998 0.1
console.log(0.1 * 0.1, calculation.accMul(0.1, 0.1)) // 乘法。0.010000000000000002 0.01
console.log(0.11 / 0.1, calculation.accDiv(0.11, 0.1)) // 除法。1.0999999999999999 1.1
```